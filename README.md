# Introdução a Engenharia de Computação

Este livro está sendo escrito pelos alunos do Curso de Engenharia da Computação da Universidade Tecnológica do Paraná. O livro é faz parte da disciplina de Introdução a Engenharia e os alunos que participaram da edição estão [listados abaixo](#Autores).

## Índice

1. [Surgimento das Calculadoras Mecânicas](capitulos/surgimento_das_calculadoras_mecanicas.md)
1. [Primeiros Computadores]()
1. [Evolução dos Computadores Pessoais e sua Interconexão]()
    - [Primeira Geração]()
1. [Computação Móvel]()
1. [Futuro]()


## Autores
Esse livro foi escrito por:

| Avatar | Nome | Nickname | Email |
| ------ | ---- | -------- | ----- |
| Avatar | Kauê Mauri de Souza | fireskazy | [kauesouza@alunos.utfpr.edu.br](mailto:kauesouza@alunos.utfpr.edu.br)
| Avatar | Pedro Henrique Gnoatto Rosa | raigame | [pedrorosa@alunos.utfpr.edu.br](mailto:pedrorosa@alunos.utfpr.edu.br)
| Avatar | João Vitor Regimund | regimundj | [joaoregimund@alunos.utfpr.edu.br](mailto:joaoregimund@alunosutfpr.edu.br)
